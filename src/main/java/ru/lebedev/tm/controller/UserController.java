package ru.lebedev.tm.controller;

import ru.lebedev.tm.App;
import ru.lebedev.tm.constant.UserAlert;
import ru.lebedev.tm.entity.Project;
import ru.lebedev.tm.entity.Task;
import ru.lebedev.tm.entity.User;
import ru.lebedev.tm.enumerated.RoleType;
import ru.lebedev.tm.exception.ProjectNotFoundException;
import ru.lebedev.tm.exception.TaskNotFoundException;
import ru.lebedev.tm.service.UserService;

import java.util.Objects;

import static ru.lebedev.tm.constant.Settings.ADMINISTRATOR;
import static ru.lebedev.tm.constant.Settings.SIMPLE_USER;
import static ru.lebedev.tm.constant.UserAlert.ACCESS_LEVEL_INSUFFICIENT;
import static ru.lebedev.tm.constant.UserAlert.NOT_LOGGED;

public class UserController extends AbstractController {
    private final UserService userService;
    private Long authUserId = 0L;

    public UserController(UserService userService){
        this.userService = userService;
    }

    public int createUser() {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        System.out.println("[CREATE USER]");
        System.out.println("PLEASE, ENTER USER FIRST NAME:");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER USER MIDDLE NAME:");
        final String middleName = scanner.nextLine();
        System.out.println("PLEASE, ENTER USER SECOND NAME:");
        final String secondName = scanner.nextLine();

        String login;
        boolean isLogin;
        do {
            isLogin = false;
            System.out.println("PLEASE, ENTER USER LOGIN:");
            login = scanner.nextLine();
            if(null != userService.findByLogin(login)) {
                isLogin = true;
                System.out.println("LOGIN EXISTS!");
            }
        } while (isLogin);

        System.out.println("PLEASE, ENTER USER PASSWORD:");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ASSIGN A USER ROLE, ENTER:");
        System.out.println("admin - ADMINISTRATOR");
        System.out.println("user - SIMPLE USER");
        RoleType userRole;
        final String role = scanner.nextLine();
        switch(role){
            case ADMINISTRATOR: userRole = RoleType.ADMIN; break;
            case SIMPLE_USER: userRole = RoleType.USER; break;

            default: {
                System.out.println("Unexpected value: " + role);
                return -1;
            }

        }
        if (Objects.isNull(userService.create(login, password, userRole, firstName,middleName, secondName))) {
            System.out.println("[FAIL]");
            return -1;
        } else {
            System.out.println("[OK]");
            return 0;
        }

    }

    public int updateUserByLogin() {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        System.out.println("[UPDATE USER]");
        System.out.println("ENTER, USER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (Objects.isNull(user)) {
            System.out.println("[FAIL]");
            return -1;
        }
        System.out.println("PLEASE, ENTER USER FIRST NAME:");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER USER MIDDLE NAME:");
        final String middleName = scanner.nextLine();
        System.out.println("PLEASE, ENTER USER SECOND NAME:");
        final String secondName = scanner.nextLine();
        if(Objects.isNull( userService.update(user.getId(), firstName, middleName, secondName, user.getLogin(),  user.getUserRole()))){
            System.out.println("[FAIL]");
            return -1;
        } else {
            System.out.println("[OK]");
            return 0;
        }

    }

    public int userChangePasswordByLogin(){
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER, USER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (Objects.isNull(user)) {
            System.out.println("[FAIL]");
            return -1;
        }
        final Long authUserId = userService.getAuthUserId();
        if (userService.authUserIsAdmin(authUserId) || authUserId.equals(user.getId())) {
            System.out.println("PLEASE, ENTER NEW PASSWORD:");
            final String newPassword = scanner.nextLine();
            if(Objects.isNull(userService.update(user.getId(), newPassword))) {
                System.out.println("[FAIL]");
                return -1;
            } else {
                System.out.println("[OK]");
                return 0;
            }

        }
        else {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }


    }

    public int userChangeRoleByLogin() {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }

        System.out.println("[CHANGE USER ROLE]");
        System.out.println("ENTER, USER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER NEW ROLE");
        System.out.println("admin - ADMINISTRATOR");
        System.out.println("user - SIMPLE USER");
        RoleType userRole;
        final String role = scanner.nextLine();
        switch (role) {
            case ADMINISTRATOR:
                userRole = RoleType.ADMIN;
                break;
            case SIMPLE_USER:
                userRole = RoleType.USER;
                break;

            default: {
                System.out.println("Unexpected value: " + role);
                return -1;
            }
        }
        if(Objects.isNull(userService.update(user.getId(), userRole))){
            System.out.println("[FAIL]");
            return -1;
        }
        System.out.println("[OK]");
        return 0;

    }

    public int clearUser() {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }

        System.out.println("[CLEAR USER]");
       userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserByLogin() {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }

        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("PLEASE, ENTER USER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.removeByLogin(login);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;

    }

    public int removeUserById() {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }

        System.out.println("[REMOVE USER BY ID]");
        System.out.println("PLEASE, ENTER USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.removeById(id);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;

    }

    public int removeUserByIndex() {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }

        System.out.println("[REMOVE USER BY INDEX]");
        System.out.println("PLEASE, ENTER USER INDEX:");
        final int index = scanner.nextInt() - 1;
        final User user = userService.removeByIndex(index);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;

    }

    public void viewUser(final User user) {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return;
        }
        if (user == null) return;
        System.out.println("[USER INFORMATION]");
        System.out.println("ID: " + user.getId());
        System.out.println("FULL NAME: " + user.getFirstName() + " " + user.getMiddleName() + " " + user.getSecondName());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD: " + user.getPassword());
        System.out.println("ROLE: " + user.getUserRole());
        System.out.println("[OK]");

    }

    public int listUser() {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        System.out.println("[LIST USER]");
        int index = 1;
        for (final User user: userService.findAll()) {
            System.out.println(
                    index + ". |" + user.getId() + " | " + user.getFirstName() + " " + user.getMiddleName()
                    + " " + user.getSecondName() + " | " + user.getLogin() + " | " + user.getUserRole()
            );
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int viewUserByLogin() {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        System.out.println("[VIEW USER]");
        System.out.println("ENTER, USER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        viewUser(user);
        return 0;

    }

    public int userAuth() {
        System.out.println("[AUTHORIZATION IN THE SYSTEM]");
        System.out.println("ENTER YOUR LOGIN:");
        final String login = scanner.nextLine();
        final User authUser = userService.findByLogin(login);
        System.out.println("ENTER YOUR PASSWORD:");
        final String userPassword = scanner.nextLine();
        if(userService.passwordVerification(authUser,userPassword)) {
            userService.setAuthUserId(authUser.getId());
            System.out.println("You are logged in.");
            System.out.println("Access level: " + authUser.getUserRole().toString());
//            System.out.println(userService.getAuthUserId());
            return 0;
        }
        else {
            System.out.println("[FAIL]");
            return -1;
        }
    }
    public int userLogout() {
        System.out.println("[SIGN OUT]");
        if (userService.noAuthUser()) {
            System.out.println(NOT_LOGGED);
            return -1;
        }
        else {
            userService.setAuthUserId(0L);
            System.out.println("[OK]");
            return 0;
        }
    }

    public int addUserToProjectByIds() throws ProjectNotFoundException {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        System.out.println("[ADD USER TO PROJECT BY ID]");
        System.out.println("PLEASE, ENTER USER ID:");
        final Long userId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        Project project = userService.addProjectToUser(projectId, userId);
        if (Objects.isNull(project)) {
            System.out.println("[FAIL]");
            return -1;
        }
        System.out.println("[OK]");
        return 0;

    }

    public int addUserToTaskByIds() throws TaskNotFoundException {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        System.out.println("[ADD USER TO TASK BY ID]");
        System.out.println("PLEASE, ENTER USER ID:");
        final Long userId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID:");
        final Long taskId = Long.parseLong(scanner.nextLine());
        Task task = userService.addTaskToUser(taskId, userId);
        if (Objects.isNull(task)) {
            System.out.println("[FAIL]");
            return -1;
        }
        System.out.println("[OK]");
        return 0;

    }

    public int removeUserFromProjectByIds(){
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        System.out.println("[REMOVE USER FROM PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final Long userId = Long.parseLong(scanner.nextLine());
        Project project = userService.removeUserFromProject(projectId, userId);
        if (Objects.isNull(project)) {
            System.out.println("[FAIL]");
            return -1;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserFromTaskByIds(){
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        System.out.println("[REMOVE USER FROM TASK BY ID]");
        System.out.println("PLEASE, ENTER USER ID:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID:");
        final Long userId = Long.parseLong(scanner.nextLine());
        Task task = userService.removeUserFromTask(projectId, userId);
        if (Objects.isNull(task)) {
            System.out.println("[FAIL]");
            return -1;
        }
        System.out.println("[OK]");
        return 0;
    }

}
