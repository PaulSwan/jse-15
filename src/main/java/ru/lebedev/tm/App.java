package ru.lebedev.tm;

import javafx.scene.chart.ScatterChart;
import ru.lebedev.tm.controller.ProjectController;
import ru.lebedev.tm.controller.SystemController;
import ru.lebedev.tm.controller.TaskController;
import ru.lebedev.tm.enumerated.RoleType;
import ru.lebedev.tm.exception.ProjectNotFoundException;
import ru.lebedev.tm.exception.TaskNotFoundException;
import ru.lebedev.tm.repository.ProjectRepository;
import ru.lebedev.tm.repository.TaskRepository;
import ru.lebedev.tm.repository.UserRepository;
import ru.lebedev.tm.service.ProjectService;
import ru.lebedev.tm.service.ProjectTaskService;
import ru.lebedev.tm.service.TaskService;
import ru.lebedev.tm.controller.UserController;
import ru.lebedev.tm.service.UserService;

import java.util.Scanner;

import static ru.lebedev.tm.constant.Settings.MAX_HISTORY_SIZE;
import static ru.lebedev.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение
 */
public class App {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final UserRepository userRepository = new UserRepository();

    private final UserService userService = new UserService(userRepository, projectRepository, taskRepository);

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ProjectController projectController = new ProjectController(projectService, userService);

    private final TaskController taskController = new TaskController(taskService, projectService, projectTaskService, userService);

    private final SystemController systemController = new SystemController();

    private final UserController userController = new UserController(userService);

     {
        projectService.create("DP 1", "DEMO PROJECT 1");
        projectService.create("DP 2", "DEMO PROJECT 2");
        taskService.create("TEST 1", "TEST TASK 1");
        taskService.create("TEST 2", "TEST TASK 2");
        userService.create("lebedev_py", "qwedsa45", RoleType.ADMIN,"Pavel" ,"Y.","Lebedev" );
        userService.create("gamma_e", "zxcdfg", RoleType.USER, "Erich", "Maria","Gamma" );
    }

    public static void main(final String[] args) /*throws ProjectNotFoundException, TaskNotFoundException */ {
        final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        // app.run(args);
        app.systemController.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            try {
                app.run(command);
            } catch (ProjectNotFoundException | TaskNotFoundException ex) {
                System.out.println(ex.getMessage());
                System.out.println("[FAIL]");
            }


        }
    }


    public void run(final String[] args) throws TaskNotFoundException, ProjectNotFoundException {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);

    }

    public int run (final String param) throws ProjectNotFoundException, TaskNotFoundException {
        if (param == null || param.isEmpty()) return -1;
        systemController.history.add(param);
        if(systemController.history.size() > MAX_HISTORY_SIZE) {
            systemController.history.poll();
        }
        switch (param) {
            case VERSION: return systemController.displayVersion();
            case ABOUT: return systemController.displayAbout();
            case HELP: return systemController.displayHelp();
            case EXIT: return systemController.displayExit();
            case HISTORY: return systemController.showHistory();

            case PROJECT_CREATE: return projectController.createProject();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_VIEW: return projectController.viewProjectByIndex();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();

            case TASK_CREATE: return taskController.createTask();
            case TASK_CLEAR: return taskController.clearTask();
            case TASK_LIST: return taskController.listTask();
            case TASK_LIST_BY_NAME: return taskController.listTaskByName();
            case TASK_VIEW: return taskController.viewTaskByIndex();
            case TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
            case TASK_REMOVE_BY_ID: return taskController.removeTaskById();
            case TASK_REMOVE_BY_INDEX: return taskController.removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX: return taskController.updateTaskByIndex();
            case TASK_ADD_TO_PROJECT_BY_IDS: return taskController.addTaskToProjectByIds();
            case TASK_REMOVE_FROM_PROJECT_BY_IDS: return taskController.removeTaskFromProjectByIds();
            case TASK_LIST_BY_PROJECT_ID: return taskController.listTaskByProjectId();

            case USER_CREATE: return userController.createUser();
            case USER_CLEAR: return userController.clearUser();
            case USER_LIST: return userController.listUser();
            case USER_VIEW: return userController.viewUserByLogin();
            case USER_REMOVE_BY_LOGIN: return userController.removeUserByLogin();
            case USER_REMOVE_BY_ID: return userController.removeUserById();
            case USER_REMOVE_BY_INDEX: return userController.removeUserByIndex();
            case USER_UPDATE_BY_LOGIN: return userController.updateUserByLogin();
            case USER_CHANGE_PASSWORD_BY_LOGIN: return userController.userChangePasswordByLogin();
            case USER_CHANGE_ROLE_BY_LOGIN: return userController.userChangeRoleByLogin();
            case USER_ADD_PROJECT_BY_IDS: return userController.addUserToProjectByIds();
            case USER_ADD_TASK_BY_IDS: return userController.addUserToTaskByIds();
            case USER_REMOVE_FROM_PROJECT_BY_IDS: return userController.removeUserFromProjectByIds();
            case USER_REMOVE_FROM_TASK_BY_IDS: return userController.removeUserFromTaskByIds();

            case USER_AUTHORIZATION: return userController.userAuth();
            case USER_LOGOUT: return userController.userLogout();

            default: return systemController.displayError();
        }
    }

 }
