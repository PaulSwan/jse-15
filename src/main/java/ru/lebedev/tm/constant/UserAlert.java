package ru.lebedev.tm.constant;

public class UserAlert {

    public static final String NOT_LOGGED = "You a not logged in the system!";
    public static final String ACCESS_LEVEL_INSUFFICIENT = "Your access level is insufficient!";

}
