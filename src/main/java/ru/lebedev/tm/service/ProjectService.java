package ru.lebedev.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.lebedev.tm.entity.Project;
import ru.lebedev.tm.exception.ProjectNotFoundException;
import ru.lebedev.tm.exception.TaskNotFoundException;
import ru.lebedev.tm.repository.ProjectRepository;

import java.util.List;
import java.util.Objects;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(String name) {
        if(StringUtils.isEmpty(name)) return null;
        return projectRepository.create(name);

    }

    public Project create(final String name, final String description) {
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(description)) return null;
        return projectRepository.create(name, description);
    }

    public Project create(final String name,final String description, final Long userId) {
        if(StringUtils.isEmpty(name) || StringUtils.isEmpty(description) || userId == null)
        return null;
        return projectRepository.create(name, description, userId);
    }

    public Project update(Long id, String name, String description) throws ProjectNotFoundException {
        if(id == null | StringUtils.isEmpty(name) | StringUtils.isEmpty(description)) {
            throw new ProjectNotFoundException("The main parameters of the project cannot be empty. Id = '" + id
                    + "'; Name = '" + name + "'; Descr. = '" + description + "'.");
        }
        return projectRepository.update(id, name, description);
    }

    public void clear() {
        projectRepository.clear();
    }

    public Project findByIndex(int index) throws ProjectNotFoundException {
        if (index < 0 || index > projectRepository.numberOfProjects() - 1) {
            throw new ProjectNotFoundException("Project with index = " + (index + 1) + "does not exist");
        }
        return projectRepository.findByIndex(index);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public List<Project> findAll(final Long userId) {
        return projectRepository.findAll(userId);
    }

    public List<Project> findAllOrderByName() {
        return projectRepository.findAllOrderByName();
    }

    public List<Project> findAllOrderByName(final Long userId) {
        return projectRepository.findAllOrderByName(userId);
    }

    public List<Project> findByName (String name) throws ProjectNotFoundException {
        if(StringUtils.isEmpty(name)) {
            throw new ProjectNotFoundException("Project with empty name does not exist.");
        }
        return projectRepository.findByName(name);
    }

    public Project findById(Long id) throws ProjectNotFoundException {
        if(id == null) {
            throw new ProjectNotFoundException("Project with empty ID does not exist.");
        }
        return projectRepository.findById(id);
    }

    public Project removeByIndex(int index) throws ProjectNotFoundException {
        if (index < 0 || index > projectRepository.numberOfProjects() - 1) {
            throw new ProjectNotFoundException("Project with index = " + (index + 1) + "does not exist");
        }
        return projectRepository.removeByIndex(index);
    }

    public Project removeById(Long id) throws ProjectNotFoundException {
        if(id == null) {
            throw new ProjectNotFoundException("Project with empty ID does not exist.");
        }
        return projectRepository.removeById(id);
    }

    public List<Project> removeByName(String name) throws ProjectNotFoundException{
        if (StringUtils.isEmpty(name)) {
            throw new ProjectNotFoundException("Project with empty name does not exist.");
        }
        return projectRepository.removeByName(name);
    }

    public final String getNameProjectById(final Long id) throws ProjectNotFoundException {
        if (Objects.isNull(id)) {
            throw new ProjectNotFoundException("Project with empty ID does not exist.");
        }
        return projectRepository.getNameProjectById(id);
    }

 }
