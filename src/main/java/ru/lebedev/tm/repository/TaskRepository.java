package ru.lebedev.tm.repository;

import ru.lebedev.tm.entity.Task;
import ru.lebedev.tm.exception.TaskNotFoundException;

import java.util.*;

public class TaskRepository {

    private static final Comparator<Task> NAME_COMPARATOR = new Comparator<Task>() {
        @Override
        public int compare(Task t1, Task t2) {
            if (t1.getName() != null && t2.getName() != null) {
                return t1.getName().compareTo(t2.getName());
            } else if (t1.getName() == null && t2.getName() != null) {
                return -1;
            } else if (t1.getName() != null && t2.getName() == null) {
                return 1;
            } else {
                return 0;
            }

        }

    };

    private List<Task> tasks = new ArrayList<>();
    private Map<String, List<Task>> tasksByName = new HashMap<>();


    public Task create(final String name) {
        final Task task = create(name, null);
        addTaskToMap(task);
        tasks.add(task);
        return task;
    }

    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        addTaskToMap(task);
        tasks.add(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        addTaskToMap(task);
        tasks.add(task);
        return task;
    }

    private void addTaskToMap(Task task) {
        String name = task.getName();
        List<Task> tasks = tasksByName.get(name);

        if (tasks == null){
          tasks = new ArrayList<>();
          tasks.add(task);
            tasksByName.put(name, tasks);
        } else {
            tasks.add(task);
        }


    }

    public Task update(final Long id, final String name, final String description) throws TaskNotFoundException {
        final Task task = findById(id);
        if (Objects.isNull(task)) return null;
        final String currentName = task.getName();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        List<Task> nameIndex = findByNameIndex(currentName);
        if (!currentName.equals(name)) {
            if (nameIndex.size() > 1) {
                nameIndex.remove(task);
                tasksByName.put(currentName, nameIndex);
            } else tasksByName.remove(currentName);
            addTaskToMap(task);
        }
        return task;
    }

    public void clear() {
        tasksByName.clear();
        tasks.clear();
    }

    public int numberOfTasks() {
        return tasks.size();
    }

    public Task findByIndex(final int index) {
        List<Task> tasks = findAllOrderByName();
        return tasks.get(index);
    }

    public Task findByName(final String name) throws TaskNotFoundException {
        for (final Task task : tasks) {
            if (task.getName().equals(name)) return task;
        }
        throw new TaskNotFoundException("Not found task with name: " + name);

    }

    public List<Task> findByNameIndex(final String name) throws TaskNotFoundException {
        List<Task> nameTasks = tasksByName.get(name);
        if(nameTasks == null) {
            throw new TaskNotFoundException("Not found task with name: " + name);
        }
        return nameTasks;

    }

    public List<Task> findByNameIndex(final String name, final Long userId) throws TaskNotFoundException {
        List<Task> nameTasks = tasksByName.get(name);
        if(nameTasks == null) {
            throw new TaskNotFoundException("Not found task with name: " + name);
        }
        List<Task> userTasks = new ArrayList<>();
        for(final Task task: nameTasks){
            if(task.getUserId().equals(userId)){
                userTasks.add(task);
            }
        }
        return userTasks;

    }

    public Task removeById(final Long id) throws TaskNotFoundException {
        final Task task = findById(id);
        if (Objects.isNull(task)) return null;
        final String name = task.getName();
        List<Task> nameTask = tasksByName.get(name);
        if(nameTask.size() > 1) {
            nameTask.remove(task);
            tasksByName.put(name, nameTask);
        } else tasksByName.remove(name);
        tasks.remove(task);
        return task;

    }

    public List<Task> removeByName(final String name) throws TaskNotFoundException{
        final List<Task> result = findByNameIndex(name);
        if (result.isEmpty()) return null;
        for(final Task task : result){
            tasks.remove(task);
        }
        tasksByName.remove(name);
        return result;

    }

    public Task removeByIndex(final int index) throws TaskNotFoundException {
        final Task task = findByIndex(index);
 //       if (Objects.isNull(task)) return null;
        final String name = task.getName();
        List<Task> nameTask = tasksByName.get(name);
        if(nameTask.size() > 1) {
            nameTask.remove(task);
            tasksByName.put(name, nameTask);
        } else tasksByName.remove(name);
        tasks.remove(task);
        return task;

    }

    public List<Task> findAllByProjectId(final Long projectId) throws TaskNotFoundException {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (task.getProjectId().equals(projectId)) {
                result.add(task);
            }
        }
        if (result == null) {
            throw new TaskNotFoundException("No tasks found for the project with ID = " + projectId +".");
        }
        return result;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long taskId) throws TaskNotFoundException {
        if (taskId == null) return null;
        for (final Task task : tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(taskId)) return task;
        }
        throw new TaskNotFoundException("For task with id =" + taskId + " not found project with id = " + projectId);

    }

    public Task findByUserIdAndId(final Long userId, final Long taskId) {
        if (Objects.isNull(taskId)) return null;
        for (final Task task : tasks) {
            final Long idUser = task.getUserId();
            if (idUser == null) continue;
            if (!idUser.equals(userId)) continue;
            if (task.getId().equals(taskId)) return task;
        }
        return null;
    }

    public Task findById(final Long id) throws TaskNotFoundException {
        for (final Task task : tasks) {
            if (task.getId().equals(id)) return task;
        }
        throw new TaskNotFoundException("Task ID = " + id + " does not exist");

    }


   public List<Task> findAll() {
        return tasks;
    }

   public List<Task> findAllOrderByName() {
        List<Task> copy = new ArrayList<>(tasks);
        Collections.sort(copy, NAME_COMPARATOR);
        return copy;
    }

    public List<Task> findAllOrderByName(final Long userId) {
       List<Task> copy = new ArrayList<>(tasks);
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId()))
                copy.add(task);
        }
        Collections.sort(copy, NAME_COMPARATOR);
        return copy;
    }


}
